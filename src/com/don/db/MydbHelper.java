package com.don.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MydbHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "Record";
	private static final int DATABASE_VERSION = 1;
	public MydbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE records (_id integer primary key autoincrement, "
		           + "record1 integer no null, record2 integer no null, record3 integer no null, record4 integer no null)");
		db.execSQL("CREATE TABLE users (_id integer primary key autoincrement, "
		           + "user1 text no null, user2 text no null,user3 text no null, user4 text no null)");
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS records");
		db.execSQL("DROP TABLE IF EXISTS users");
        onCreate(db);
	}
}
