package com.don.activity;

import com.don.db.HistoryDbHelper;
import com.don.db.MydbHelper;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class ShowRecord extends Activity{
	
	TextView username1,username2,username3,username4;
	TextView userrecord1,userrecord2,userrecord3,userrecord4;

	private static String DATABASE_TABLE = "records";
	private SQLiteDatabase db;
	private SQLiteDatabase historyDb;
    private MydbHelper dbHelper;
    private HistoryDbHelper historyDbHelper; 
    
	Integer[] userrecord = {0,0,0,0};
	String[] users = new String[4];
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.show_record);
  
    dbHelper = new MydbHelper(this);
    db = dbHelper.getWritableDatabase();
    historyDbHelper = new HistoryDbHelper(this);
    historyDb = historyDbHelper.getWritableDatabase();
    
    
    String[] colNames=new String[]{"record1","record2","record3","record4"};
	Cursor c = db.query(DATABASE_TABLE, colNames,null, null, null, null,null);
	c.moveToFirst();
	if(c.getCount()>0){
	for (int i = 0; i < c.getCount(); i++) {
		for(int j = 0; j<4 ;j++){
			userrecord[j] = userrecord[j] + Integer.parseInt(c.getString(c.getColumnIndex(colNames[j])));
		}
		c.moveToNext();
//		userrecord[0] = userrecord[0] + Integer.parseInt(c.getString(c.getColumnIndex(colNames[0])));
//		userrecord[1] = userrecord[1] + Integer.parseInt(c.getString(c.getColumnIndex(colNames[1])));
//		userrecord[2] = userrecord[2] + Integer.parseInt(c.getString(c.getColumnIndex(colNames[2])));
//		userrecord[3] = userrecord[3] + Integer.parseInt(c.getString(c.getColumnIndex(colNames[3])));
		
	}
	}
    
    username1 = (TextView)findViewById(R.id.username1);
    username2 = (TextView)findViewById(R.id.username2);
    username3 = (TextView)findViewById(R.id.username3);
    username4 = (TextView)findViewById(R.id.username4);
    userrecord1 = (TextView)findViewById(R.id.userrecord1);
    userrecord2 = (TextView)findViewById(R.id.userrecord2);
    userrecord3 = (TextView)findViewById(R.id.userrecord3);
    userrecord4 = (TextView)findViewById(R.id.userrecord4);
    
    
    
//    add_button = (Button)findViewById(R.id._addrecord);
//    add_button.setOnClickListener(add);
//    
//    home_button = (Button)findViewById(R.id._home);
//    home_button.setOnClickListener(home);
//    
//    list_button = (Button)findViewById(R.id._list);
//    list_button.setOnClickListener(list);
//    
//    save_button = (Button)findViewById(R.id._save);
//    save_button.setOnClickListener(save);
    
	Cursor c2 = db.query("users", null ,null, null, null, null,null);
	
	c2.moveToFirst();
	users[0] = c2.getString(1);
	users[1] = c2.getString(2);
	users[2] = c2.getString(3);
	users[3] = c2.getString(4);
    username1.setText(c2.getString(1));
    username2.setText(c2.getString(2));
    username3.setText(c2.getString(3));
    username4.setText(c2.getString(4));
    
    if(c.getCount()>0){
    	userrecord1.setText(userrecord[0].toString());
    	userrecord2.setText(userrecord[1].toString());
    	userrecord3.setText(userrecord[2].toString());
    	userrecord4.setText(userrecord[3].toString());
    	userrecord1.setTextColor(userrecord[0]>0? Color.GREEN : userrecord[0]==0 ? Color.WHITE : Color.RED);
    	userrecord2.setTextColor(userrecord[1]>0? Color.GREEN : userrecord[1]==0 ? Color.WHITE : Color.RED);
    	userrecord3.setTextColor(userrecord[2]>0? Color.GREEN : userrecord[2]==0 ? Color.WHITE : Color.RED);
    	userrecord4.setTextColor(userrecord[3]>0? Color.GREEN : userrecord[3]==0 ? Color.WHITE : Color.RED);
    }
    }
	
	@Override
	protected void onStop() {
		super.onStop();
		db.close(); // ������Ʈw
		historyDb.close();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		db = dbHelper.getWritableDatabase();
		historyDb = historyDbHelper.getWritableDatabase();
	}
	
	/*Menu Setting*/
	private static final int menuHome = 0;
	private static final int menuAdd = 1;
	private static final int menuList = 2;
	private static final int menuSave = 3;
	
	public boolean onCreateOptionsMenu(Menu menu){
		menu.add(Menu.NONE,menuHome,0,"Home");
		menu.add(Menu.NONE,menuAdd,0,"Add Record");
		menu.add(Menu.NONE,menuList,0,"List Record");
		menu.add(Menu.NONE,menuSave,0,"Save & Exit");
		return super.onCreateOptionsMenu(menu);
	}
	
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()) {
		case menuHome:{
			ShowRecord.this.finish();
			break;
			}
		case menuAdd:{
			ShowRecord.this.finish();
    		Intent index = new Intent(ShowRecord.this, AddRecord.class);
        	startActivity(index);
			break;	
			}
		case menuList:{
			Intent index = new Intent(ShowRecord.this, ListRecord.class);
        	startActivity(index);
			break;
			}
		case menuSave:{
			ContentValues cv = new ContentValues();
    		cv.put("user1", users[0]);
	        cv.put("user2", users[1]);
	        cv.put("user3", users[2]);
	        cv.put("user4", users[3]);
    		cv.put("record1", userrecord[0]);
	        cv.put("record2", userrecord[1]);
	        cv.put("record3", userrecord[2]);
	        cv.put("record4", userrecord[3]);
	        historyDb.insert("history", null, cv);
	        Toast.makeText(ShowRecord.this,"Game End! Record save as history! ", Toast.LENGTH_LONG).show();
	        ShowRecord.this.finish();
			break;
			}
		}
		return super.onOptionsItemSelected(item);
	}
	
	
}
