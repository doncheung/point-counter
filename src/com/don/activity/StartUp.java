package com.don.activity;


import com.don.db.MydbHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Button;
import android.widget.Toast;

public class StartUp extends Activity {
    /** Called when the activity is first created. */
	
	private SQLiteDatabase db;
    private MydbHelper dbHelper;
    
    
	private Button start_button;
	private Button continue_button;
	private Button history_button;
	private Button exit_button;
	private OnClickListener start;
	private OnClickListener continues;
	private OnClickListener history;
	private OnClickListener exit;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        dbHelper = new MydbHelper(this);
        db = dbHelper.getWritableDatabase();
        Cursor c = db.query("users", null,null, null, null, null,null);
        
        if(c.getCount()>0){
        	new UserAccount().started=true;
        }
        
        
        start = new OnClickListener(){
	        public void onClick(View view) {
	        	Intent index = new Intent(StartUp.this, StartCount.class);
	        	startActivity(index);
	        }
        };
        
        continues = new OnClickListener(){
	        public void onClick(View view) {
	        	
	        	
	        	if(new UserAccount().started){
		        	Intent index = new Intent(StartUp.this, ShowRecord.class);
		        	startActivity(index);
	        	}
	        	else{
	        		 dialog2();
	        	}
	        }
        };
        
        history = new OnClickListener(){
	        public void onClick(View view) {
	        	Intent index = new Intent(StartUp.this, ShowHistory.class);
	        	startActivity(index);
	        }
        };
        
        exit = new OnClickListener(){
	        public void onClick(View view) {
	        	dialog();
	        }
        };
        
        
        
        start_button = (Button)findViewById(R.id.start);
        start_button.setOnClickListener(start);
                
        continue_button = (Button)findViewById(R.id.continues);
        continue_button.setOnClickListener(continues);
        
        history_button = (Button)findViewById(R.id.history);
        history_button.setOnClickListener(history);
        
        exit_button = (Button)findViewById(R.id.exit);
        exit_button.setOnClickListener(exit);
        
    }
    
    protected void dialog(){
    	AlertDialog.Builder alert = new Builder(StartUp.this);
		alert.setTitle("Exit");
		alert.setMessage("Do you confirm to exit Point Counter?");
		alert.setPositiveButton("Yes",new DialogInterface.OnClickListener(){
		    	 public void onClick(DialogInterface dialog, int which){
		    		 dialog.dismiss();
		    		 StartUp.this.finish();
		    	 }
	     	});
		alert.setNegativeButton("No",new DialogInterface.OnClickListener(){
		    	 public void onClick(DialogInterface dialog, int which){
				   dialog.dismiss();
		    	 }
	     	});
		alert.create().show();
    }
   
    protected void dialog2(){
    	AlertDialog.Builder alert = new Builder(StartUp.this);
		alert.setTitle("Continue Failed!");
		alert.setMessage("You must start the counter first.");
		alert.setPositiveButton("Yes",new DialogInterface.OnClickListener(){
		    	 public void onClick(DialogInterface dialog, int which){
		    		 dialog.dismiss();
		    		 //StartUp.this.finish();
		    		 Intent index = new Intent(StartUp.this, StartCount.class);
			        startActivity(index);
		    	 }
	     	});
		alert.create().show();
    }
    
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	dialog();
                return false;
        }
    return super.onKeyDown(keyCode, event);
    }
    
    @Override
	protected void onStop() {
		super.onStop();
		db.close();
	}
}