package com.don.activity;

import com.don.db.MydbHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;


public class StartCount extends Activity {

	EditText user1, user2, user3, user4;
	Button submit_button;
	Button reset_button;
	
	private static String DATABASE_TABLE = "records";
	private SQLiteDatabase db;
    private MydbHelper dbHelper;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.add_user);
    
    dbHelper = new MydbHelper(this);
    db = dbHelper.getWritableDatabase();
    
    user1 = (EditText)findViewById(R.id.user1);
    user2 = (EditText)findViewById(R.id.user2);
    user3 = (EditText)findViewById(R.id.user3);
    user4 = (EditText)findViewById(R.id.user4);
    
    int maxLength = 8;
    InputFilter[] FilterArray = new InputFilter[1];
    FilterArray[0] = new InputFilter.LengthFilter(maxLength);
    user1.setFilters(FilterArray);
    user2.setFilters(FilterArray);
    user3.setFilters(FilterArray);
    user4.setFilters(FilterArray);
    
    submit_button = (Button)findViewById(R.id.submit);
    reset_button = (Button)findViewById(R.id.reset);
	}
	
	public static void hidden(Activity activity) {
	    InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
	    inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		db.close();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		db = dbHelper.getWritableDatabase();
	}
	
	protected void dialog(){
    	AlertDialog.Builder alert = new Builder(StartCount.this);
		alert.setTitle("User Error!");
		alert.setMessage("Player name is not ready!");
		alert.setPositiveButton("OK",new DialogInterface.OnClickListener(){
		    	 public void onClick(DialogInterface dialog, int which){
		    		 dialog.dismiss();
		    	 }
	     	});
		alert.create().show();
    }
	public void submitUser(View v){
		
		String player1 = user1.getText().toString();
		String player2 = user2.getText().toString();
		String player3 = user3.getText().toString();
		String player4 = user4.getText().toString();
		if(player1.equals("")||player2.equals("")||player3.equals("")||player4.equals("")){
			dialog();
		}
		else{
		dbHelper.onUpgrade(db, 1, 1);
		UserAccount.userName[0]= player1;
		UserAccount.userName[1]= player2;
		UserAccount.userName[2]= player3;
		UserAccount.userName[3]= player4;
		long id; 
		ContentValues cv = new ContentValues();
   	    cv.put("user1", player1);
        cv.put("user2", player2);
        cv.put("user3", player3);
        cv.put("user4", player4);
        id = db.insert("users", null, cv); 
		StartCount.this.finish();
		UserAccount.started = true;
		Intent index = new Intent(StartCount.this, ShowRecord.class);
    	startActivity(index);
		}
	}
	public void reset(View v){
		user1.setText("");
		user2.setText("");
		user3.setText("");
		user4.setText("");
	}
}
