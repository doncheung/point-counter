package com.don.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.don.db.MydbHelper;

public class AddRecord extends Activity {
	
	private static String DATABASE_TABLE = "records";
	private SQLiteDatabase db;
    private MydbHelper dbHelper;
    
    TextView existuser1,existuser2,existuser3,existuser4;
    EditText record1, record2, record3, record4;
    Integer[] records;
	
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.add_record);
	    dbHelper = new MydbHelper(this);
        db = dbHelper.getWritableDatabase();
        existuser1 = (TextView)findViewById(R.id.existuser1);
        existuser2 = (TextView)findViewById(R.id.existuser2);
        existuser3 = (TextView)findViewById(R.id.existuser3);
        existuser4 = (TextView)findViewById(R.id.existuser4);
        String[] colNames2=new String[]{"user1","user2","user3","user4"};
    	Cursor c2 = db.query("users", colNames2,null, null, null, null,null);
    	c2.moveToFirst();
    	existuser1.setText(c2.getString(c2.getColumnIndex(colNames2[0])));
        existuser2.setText(c2.getString(c2.getColumnIndex(colNames2[1])));
        existuser3.setText(c2.getString(c2.getColumnIndex(colNames2[2])));
        existuser4.setText(c2.getString(c2.getColumnIndex(colNames2[3])));
        
        record1 = (EditText)findViewById(R.id.record1);
        record2 = (EditText)findViewById(R.id.record2);
        record3 = (EditText)findViewById(R.id.record3);
        record4 = (EditText)findViewById(R.id.record4);
        
        records= new Integer[4];
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		db.close();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		db = dbHelper.getWritableDatabase();
	}
	
	public void submit_Click(View view) {
		String strRecord1 = record1.getText().toString();
		String strRecord2 = record2.getText().toString();
		String strRecord3 = record3.getText().toString();
		String strRecord4 = record4.getText().toString();
		if(strRecord1.equals("") && strRecord2.equals("") && strRecord3.equals("") && strRecord4.equals("")){
			dialog(1);
		}
		else{
			long id; 
			
			ContentValues cv = new ContentValues();
			records[0] = strRecord1.equals("") ||strRecord1==null ? 0 : Integer.parseInt(strRecord1);
			records[1] = strRecord2.equals("") || strRecord2==null ? 0 : Integer.parseInt(strRecord2);
			records[2] = strRecord3.equals("") || strRecord3==null ? 0 : Integer.parseInt(strRecord3);
			records[3] = strRecord4.equals("") || strRecord4==null ? 0 : Integer.parseInt(strRecord4);
	        boolean isLower = false;
			for(int i=0;i<records.length;i++){
				if(records[i]>10000 || records[i]<-10000){
					dialog(2);
					break;
				}
				else{
					isLower = true;
				}
			}
			if(isLower){
				int recordTotal = 0;
				for(int record : records){
		    	   recordTotal += record;
				}
				if(recordTotal!=0){
					dialog(3);
				}else{
					cv.put("record1", records[0]);
			        cv.put("record2", records[1]);
			        cv.put("record3", records[2]);
			        cv.put("record4", records[3]);
			        id = db.insert(DATABASE_TABLE, null, cv); 
			        AddRecord.this.finish();
			        Intent index = new Intent(AddRecord.this, ShowRecord.class);
			    	startActivity(index);
				}
			}
			
		}
		
   }
	
	protected void dialog(Integer cases){
    	AlertDialog.Builder alert = new Builder(AddRecord.this);
		alert.setTitle("Data Error!");
		if(cases==1){
			alert.setMessage("Please input record!");
		}
		else if(cases==2){
			alert.setMessage("Record is larger than 10000!");
		}
		else{
			alert.setMessage("Records Total is not equals 0!");
		}
		alert.setPositiveButton("OK",new DialogInterface.OnClickListener(){
		    	 public void onClick(DialogInterface dialog, int which){
		    		 dialog.dismiss();
		    	 }
	     	});
		alert.create().show();
    }
	
	public void reset(View v){
		record1.setText("");
		record2.setText("");
		record3.setText("");
		record4.setText("");
	};
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	AddRecord.this.finish();
        	Intent index = new Intent(AddRecord.this, ShowRecord.class);
        	startActivity(index);
                return false;
        }
    return super.onKeyDown(keyCode, event);
    }
    
}
