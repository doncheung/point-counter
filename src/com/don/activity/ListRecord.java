package com.don.activity;

import java.util.ArrayList;
import java.util.HashMap;

import com.don.db.MydbHelper;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class ListRecord extends Activity{
	
	private static String DATABASE_TABLE = "records";
	private SQLiteDatabase db;
    private MydbHelper dbHelper;
	
    private LinearLayout layout_exist_list;
    private TableLayout existTableList;
    //private TableLayout layout_new_list;
    private TableRow tableRow;
    private TextView tv;
    private TextView tv1;
    private TextView tv2;
    private TextView tv3;
    private TextView tv4;
    
    private String[] user = UserAccount.userName;
    @Override
	public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.list_view);
    
    dbHelper = new MydbHelper(this);
    db = dbHelper.getWritableDatabase();
    
    
//    ListView list = (ListView)findViewById(R.id.listView);
//    
//    ArrayList<HashMap<String,Object>> listItem = new ArrayList<HashMap<String,Object>>();
//    
//	String[] colNames2=new String[]{"user1","user2","user3","user4"};
//	Cursor c2 = db.query("users", colNames2,null, null, null, null,null);
//	c2.moveToFirst();
//	
//	HashMap<String,Object> map0 = new HashMap<String,Object>();
//	map0.put("no","");
//	map0.put("record1", c2.getString(c2.getColumnIndex(colNames2[0])));
//	map0.put("record2", c2.getString(c2.getColumnIndex(colNames2[1])));
//	map0.put("record3", c2.getString(c2.getColumnIndex(colNames2[2])));
//	map0.put("record4", c2.getString(c2.getColumnIndex(colNames2[3])));
//	listItem.add(map0);
//	
//	String[] colNames=new String[]{"record1","record2","record3","record4"};
//	Cursor c = db.query(DATABASE_TABLE, colNames,null, null, null, null,null);
//	c.moveToFirst();
//	for (int i = 0; i < c.getCount(); i++) {
//	HashMap<String,Object> map = new HashMap<String,Object>();
//	map.put("no",i+1);
//	map.put("record1", c.getString(c.getColumnIndex(colNames[0])));
//	map.put("record2", c.getString(c.getColumnIndex(colNames[1])));
//	map.put("record3", c.getString(c.getColumnIndex(colNames[2])));
//	map.put("record4", c.getString(c.getColumnIndex(colNames[3])));
//	listItem.add(map);
//	c.moveToNext();
//	}
    
//    SimpleAdapter listItemAdapter = new SimpleAdapter(this,listItem,R.layout.list_view,
//    		new String[]{"no","record1","record2","record3","record4"},
//    		new int[]{R.id.no,R.id.record1,R.id.record2,R.id.record3,R.id.record4});
//    
//    list.setAdapter(listItemAdapter);
//    
//	
    try{
    	existTableList = (TableLayout)findViewById(R.id.list_info);
        String[] colNames2=new String[]{"user1","user2","user3","user4"};
    	Cursor c2 = db.query("users", colNames2,null, null, null, null,null);
    	c2.moveToFirst();
        
        tv = (TextView)findViewById(R.id.no);
        tv.setText(" ");
    	tv1 = (TextView)findViewById(R.id.record1);
        tv1.setText( c2.getString(c2.getColumnIndex(colNames2[0])));
       
        tv2 = (TextView)findViewById(R.id.record2);
        tv2.setText( c2.getString(c2.getColumnIndex(colNames2[1])));
       
        tv3 = (TextView)findViewById(R.id.record3);
        tv3.setText( c2.getString(c2.getColumnIndex(colNames2[2])));
        
        tv4 = (TextView)findViewById(R.id.record4);
        tv4.setText( c2.getString(c2.getColumnIndex(colNames2[3])));
        
//        tableRow = new TableRow(ListRecord.this);
//        tableRow.addView(tv1);
//        tableRow.addView(tv2);
//        tableRow.addView(tv3);
//        tableRow.addView(tv4);
//        existTableList.addView(tableRow);
        
        
        
        String[] colNames=new String[]{"record1","record2","record3","record4"};
    	Cursor c = db.query(DATABASE_TABLE, colNames,null, null, null, null,null);
    	c.moveToFirst();
    		if(c.getCount()>0){
    			for (int i = 0; i < c.getCount(); i++) {
    				TextView recordTv = new TextView(ListRecord.this);
    				int round = i+1; 
    				recordTv.setText(""+round);
    				recordTv.setTextSize(50);
    			    TextView recordTv1 = new TextView(ListRecord.this);
    			    int record1 = c.getInt(c.getColumnIndex(colNames[0]));
    			    recordTv1.setText(""+record1);
    			    recordTv1.setTextColor(record1>0 ? Color.GREEN : record1==0 ? Color.WHITE : Color.RED);
    			    recordTv1.setTextSize(20);
    			    recordTv1.setGravity(17);
    			    TextView recordTv2 = new TextView(ListRecord.this);
    			    int record2 = c.getInt(c.getColumnIndex(colNames[1]));
    			    recordTv2.setText(""+record2);
    			    recordTv2.setTextColor(record2>0 ? Color.GREEN : record2==0 ? Color.WHITE : Color.RED);
    			    recordTv2.setTextSize(20);
    			    recordTv2.setGravity(17);
    			    TextView recordTv3 = new TextView(ListRecord.this);
    			    int record3 = c.getInt(c.getColumnIndex(colNames[2]));
    			    recordTv3.setText(""+record3);
    			    recordTv3.setTextColor(record3>0 ? Color.GREEN : record3==0 ? Color.WHITE : Color.RED);
    			    recordTv3.setTextSize(20);
    			    recordTv3.setGravity(17);
    			    TextView recordTv4 = new TextView(ListRecord.this);
    			    int record4 = c.getInt(c.getColumnIndex(colNames[3]));
    			    recordTv4.setText(""+record4);
    			    recordTv4.setTextColor(record4>0 ? Color.GREEN : record4==0 ? Color.WHITE : Color.RED);
    			    recordTv4.setTextSize(20);
    			    recordTv4.setGravity(17);
    			    TableRow recordTableRow = new TableRow(ListRecord.this);
    			    recordTableRow.addView(recordTv);
    			    recordTableRow.addView(recordTv1);
    			    recordTableRow.addView(recordTv2);
    			    recordTableRow.addView(recordTv3);
    			    recordTableRow.addView(recordTv4);
    			    existTableList.addView(recordTableRow);
    				c.moveToNext();
    			}
    			//layout_new_list.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.FILL_PARENT));
    			//layout_new_list.setStretchAllColumns(true);
    			
    		}
    }catch(Exception e){
    	e.printStackTrace();
    }
    
	}
    
    @Override
	protected void onStop() {
		super.onStop();
		db.close();
	}

}
