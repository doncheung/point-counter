package com.don.activity;

import java.util.ArrayList;
import java.util.HashMap;

import com.don.db.HistoryDbHelper;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class ShowHistory extends Activity{
	
	
	
	private static String DATABASE_TABLE = "history";
	private SQLiteDatabase db;
    private HistoryDbHelper dbHelper;
	
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.history);
    
    dbHelper = new HistoryDbHelper(this);
    db = dbHelper.getWritableDatabase();
    ListView list = (ListView)findViewById(R.id.historyView);
    ArrayList<HashMap<String,Object>> listItem = new ArrayList<HashMap<String,Object>>();
    Cursor c = db.query(DATABASE_TABLE, null ,null, null, null, null,null);
	c.moveToFirst();

	if(c.getCount()>0){
	for (int i = 0; i < c.getCount(); i++) {
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("historyUser1", c.getString(1));
		map.put("historyUser2", c.getString(2));
		map.put("historyUser3", c.getString(3));
		map.put("historyUser4", c.getString(4));
		map.put("historyRecord1", c.getString(5));
		map.put("historyRecord2", c.getString(6));
		map.put("historyRecord3", c.getString(7));
		map.put("historyRecord4", c.getString(8));
		listItem.add(map);
		c.moveToNext();
	}
	}
	SimpleAdapter listItemAdapter = new SimpleAdapter(this,listItem,R.layout.history_view,
	new String[]{"historyUser1","historyUser2","historyUser3","historyUser4","historyRecord1","historyRecord2","historyRecord3","historyRecord4"},
	new int[]{R.id.historyUser1,R.id.historyUser2,R.id.historyUser3,R.id.historyUser4,R.id.historyRecord1,R.id.historyRecord2,R.id.historyRecord3,R.id.historyRecord4});
    
    list.setAdapter(listItemAdapter);
    }
	
	public void clear_click(View view) {
		dbHelper.onUpgrade(db, 1, 1);
		ShowHistory.this.finish();
		Intent index = new Intent(ShowHistory.this, ShowHistory.class);
    	startActivity(index);
	};
	
	@Override
	protected void onStop() {
		super.onStop();
		db.close(); // ������Ʈw
	}
	
}
